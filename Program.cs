using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fleck;
using System.Diagnostics;


namespace amtf_dxf
{
    static class Program
    {
        public static Form1 窗口;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            开启服务();
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(窗口 = new Form1());

            //netDxf.Entities.Polyline2D.Vertexes.Bulge //群友 关心 给的导出天工提示
            static void 开启服务()
            {
                FleckLog.Level = LogLevel.Debug;
                var allSockets = new List<IWebSocketConnection>();
                var server = new WebSocketServer("ws://0.0.0.0:8181");
                server.Start(socket =>
                {
                    socket.OnOpen = () =>
                    {
                        Console.WriteLine("Open!");
                        allSockets.Add(socket);
                    };
                    socket.OnClose = () =>
                    {
                        Console.WriteLine("Close!");
                        allSockets.Remove(socket);
                    };
                    socket.OnMessage = message =>
                    {
                        Console.WriteLine(message);
                        //窗口.richTextBox1.Text += ("处理中：" + message + "\n");

                        string[] ss = message.Split('|');
                        string dxf全名 = ss[1];
                        string 板厚mm = ss[2];
                        string 计数 = ss[3];
                        string 封边信息 = ss[4];
                        switch (ss[0])
                        {
                            case "调用amtf_dxf":
                                allSockets.ToList().ForEach(s => s.Send("收到的指示："+ message));
                                //窗口.richTextBox1.AppendText("收到的指示： " + message + " \n");

                                amtf_dxf.处理dxf图层(dxf全名, 板厚mm, 计数, 封边信息);
                                break;
                            case "测试连接":
                                allSockets.ToList().ForEach(s => s.Send("amtf_dxf  连接成功……"));
                                break;
                            default:
                                break;
                        }
                        //allSockets.ToList().ForEach(s => s.Send("Echo: " + message));
                    };
                });

                Console.WriteLine("我在等 sketchup 转 dxf 给我处理，暂时不要把我关闭！");

                //var input = Console.ReadLine();
                //while (input != "exit")
                //{
                //    foreach (var socket in allSockets.ToList())
                //    {
                //        socket.Send(input);
                //    }
                //    input = Console.ReadLine();
                //}

            }

        }
    }
}
